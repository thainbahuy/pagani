// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueRouter from 'vue-router'
import {routes} from './router/index'

Vue.use(VueAxios, axios);
Vue.use(VueRouter);
//component
//Vue.component('ListProductComponent',require('./components/ListProductComponent.vue').default); sai cu phap

Vue.component('editproduct-component',require('./components/EditProductComponent.vue').default);
Vue.component('listproduct-component',require('./components/ListProductComponent.vue').default);

const router = new VueRouter({
  mode: 'history',
  routes,
});

new Vue({
  el: '#app',
  router,
})




// Vue.config.productionTip = false
