import editproduct from '../components/EditProductComponent'
import listproduct from '../components/ListProductComponent'

export const routes =
    [
        {
            path: '/', ///path của route
            name: 'product', // tên route
            component: listproduct // component route sử dụng
        },
        {
            path: '/edit/:id', ///path của route
            name: 'edit', // tên route
            component: editproduct // component route sử dụng
        },

    ];
